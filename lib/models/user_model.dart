import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class UserModel {
  String name;
  String firstName;
  String lastName;
  String email;
  String pictureUrl;
  String id;

  UserModel(
      {this.name,
      this.firstName,
      this.lastName,
      this.email,
      this.pictureUrl,
      this.id});

  UserModel.fromFacebookJson(Map<String, dynamic> json) {
    name = json['name'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    pictureUrl = json['picture']['data']['url'];
    id = json['id'];
  }

  UserModel.fromAppleJson(AuthorizationCredentialAppleID credential) {
    name = "${credential.givenName} ${credential.familyName}";
    firstName = credential.givenName;
    lastName = credential.familyName;
    email = credential.email;
    id = credential.userIdentifier;
  }

  UserModel.fromGoogleJson(GoogleSignInAccount googleSignInAccount) {
    name = googleSignInAccount.displayName;
    email = googleSignInAccount.email;
    pictureUrl = googleSignInAccount.photoUrl;
    id = googleSignInAccount.id;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['pictureUrl'] = this.pictureUrl;
    data['id'] = this.id;
    return data;
  }
}
