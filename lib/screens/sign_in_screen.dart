import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:tip_top/models/user_model.dart';
import 'package:tip_top/parsers/login_parser.dart';
import 'package:tip_top/screens/home_screen.dart';
import 'package:tip_top/utils/app_strings.dart';
import 'package:tip_top/utils/constants.dart';
import 'package:tip_top/utils/layout_constant.dart';
import 'package:tip_top/utils/preference.dart';
import 'package:tip_top/utils/text_button_widgtes.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  var facebookLogin = FacebookLogin();

  final GoogleSignIn _googleSignIn = GoogleSignIn( scopes: [
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],);

  bool isError = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        body: Container(
          decoration: LayoutConstant.containerDecoration(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(),
              Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          child: Hero(
                              tag: "app_logo",
                              child: SvgPicture.asset('images/main_logo.svg',
                                  height: 70))),
                    ],
                  )),
              Expanded(flex: 2, child: loginSocial())
            ],
          ),
        ));
  }

  Widget loginSocial() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [
          (Platform.isAndroid)
              ? InkResponse(
                  onTap: logInWithGoogle,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white70, width: 1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16, bottom: 16),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                              child: SvgPicture.asset('images/google_icon.svg',
                                  height: 28, width: 28)),
                          Expanded(
                              flex: 2,
                              child: SubTitleText(text: 'Sign In With Google'))
                        ],
                      ),
                    ),
                  ),
                )
              : Container(),
          SizedBox(height: 4),
          InkResponse(
            onTap: logInWithFacebook,
            child: Card(
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        child: SvgPicture.asset('images/facebook_icon.svg',
                            height: 28, width: 28)),
                    Expanded(
                        flex: 2,
                        child: SubTitleText(text: 'Sign In With Facebook'))
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 4),
          (Platform.isIOS)
              ? InkResponse(
                  onTap: logInWithApple,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white70, width: 1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16, bottom: 16),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                              child: SvgPicture.asset('images/apple_icon.svg',
                                  height: 28, width: 28)),
                          Expanded(
                              flex: 2,
                              child: SubTitleText(text: 'Sign In With Apple'))
                        ],
                      ),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  ///login with apple
  void logInWithApple() async {
    final credential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
    );

    if (credential != null) {
      UserModel userModel = UserModel.fromAppleJson(credential);
      Preference.setString(Constants.USERID, userModel.id);
      Preference.setString(Constants.USEREMAIL, userModel.email);
      Preference.setString(Constants.USERNAME, userModel.name);
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (_) => HomeScreen()));
    } else {
      LayoutConstant.showSnackBar(scaffoldKey, 'Sign In Failed.', context);
    }
  }

  //login with facebook
  void logInWithFacebook() async {
    try {
      var facebookLoginResult = await facebookLogin.logIn(['email']);

      switch (facebookLoginResult.status) {
        case FacebookLoginStatus.error:
          LayoutConstant.showSnackBar(scaffoldKey, 'Sign Error.', context);
          break;
        case FacebookLoginStatus.cancelledByUser:
          LayoutConstant.showSnackBar(scaffoldKey, 'Sign Cancelled.', context);
          break;
        case FacebookLoginStatus.loggedIn:
          var graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult.accessToken.token}');

          var profile = json.decode(graphResponse.body);
          if (profile != null && profile['id'] != null) {
            UserModel userModel = UserModel.fromFacebookJson(profile);
            Preference.setString(Constants.USERPROFILE, userModel.pictureUrl);
            Preference.setString(Constants.USEREMAIL, userModel.email);
            Preference.setString(Constants.USERNAME, userModel.name);
            Preference.setString(Constants.USERID, userModel.id);


            Map postUser = apiParams(profile);
            Map result = await LoginParser.postLogin(
                Constants.appUrl + "/add_user.php", postUser);

            if (!result["isError"]) {
              UserModel userModel = result["value"];

              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (_) => HomeScreen()));
              print(result["value"]);
            } else {
              scaffoldKey.currentState.showSnackBar(
                  SnackBar(content: Text(AppString.somethingWantWrong)));
              isError = true;
            }
          } else {
            LayoutConstant.showSnackBar(scaffoldKey, 'Sign Error.', context);
          }

          break;
      }
    } catch (e) {
      print(e);
    }
  }

  Map apiParams(profile) {
    Map postUser = {
      "first_name": profile['first_name'],
      "last_name": profile['last_name'],
      "email_id": profile['email'],
      "country_code": "",
      "mobile": "",
      "device_type": Platform.isIOS ? "I" : "A",
      "device_token": "ABCD",
      "profile_pic": profile['picture']['data']['url'],
      "social_key": profile['id'],
    };
    return postUser;
  }

  //login with google
  void logInWithGoogle() async {
    try {
      GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
      if (googleSignInAccount != null &&
          googleSignInAccount.id != null &&
          googleSignInAccount.id.isNotEmpty) {
        UserModel userModel = UserModel.fromGoogleJson(googleSignInAccount);
        Preference.setString(Constants.USERPROFILE, userModel.pictureUrl);
        Preference.setString(Constants.USERNAME, userModel.name);
        Preference.setString(Constants.USERID, userModel.id);
        Preference.setString(Constants.USEREMAIL, userModel.email);

        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (_) => HomeScreen()));
      }
    } catch (e) {
      print(e);
    }
  }
}
