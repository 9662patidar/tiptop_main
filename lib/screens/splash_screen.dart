import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tip_top/screens/home_screen.dart';
import 'package:tip_top/screens/sign_in_screen.dart';
import 'package:tip_top/utils/constants.dart';
import 'package:tip_top/utils/layout_constant.dart';
import 'package:tip_top/utils/navigation_constant.dart';
import 'package:tip_top/utils/preference.dart';
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 2)).then((value) {
      if (Preference.prefs!=null && Preference.prefs.getString(Constants.USERID) != null &&
          Preference.getString(Constants.USERID).isNotEmpty) {
        NavigationConstant.navigationPushReplacement(context, HomeScreen());
      } else {
        NavigationConstant.navigationPushReplacement(context, SignInScreen());
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: LayoutConstant.containerDecoration(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Hero(
                tag: "app_logo",
                child: SvgPicture.asset('images/main_logo.svg', height: 100)),
          ],
        ),
      ),
    );
  }
}
