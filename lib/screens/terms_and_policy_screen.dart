import 'package:flutter/material.dart';
import 'package:tip_top/utils/app_strings.dart';
import 'package:tip_top/utils/layout_constant.dart';
import 'package:tip_top/utils/text_button_widgtes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class TermsAndPolicyScreen extends StatefulWidget {
  final String appTitle;

    TermsAndPolicyScreen({Key key, this.appTitle}) : super(key: key);
  @override
  _TermsAndPolicyScreenState createState() => _TermsAndPolicyScreenState();
}

class _TermsAndPolicyScreenState extends State<TermsAndPolicyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          decoration: LayoutConstant.containerDecoration(),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              centerTitle: true,
              leading: InkResponse(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Card(
                    child: Icon(Icons.arrow_back),
                    margin: EdgeInsets.all(10),
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.w),
                    )),
              ),
              title: HeadlineText(
                  text: widget.appTitle, fontWeight: FontWeight.w500),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Body1Text(text: widget.appTitle==AppString.tAndC?AppString.tAndCText:AppString.privacyText,fontWeight: FontWeight.w400),
              ),
            ),
          ),
        ));
  }
}
