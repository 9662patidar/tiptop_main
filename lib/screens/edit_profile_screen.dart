import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tip_top/utils/app_strings.dart';
import 'package:tip_top/utils/color_constant.dart';
import 'package:tip_top/utils/constants.dart';
import 'package:tip_top/utils/layout_constant.dart';
import 'package:tip_top/utils/preference.dart';
import 'package:tip_top/utils/text_button_widgtes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  TextEditingController fullNameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();

  @override
  void initState() {
    fullNameController.text = Preference.prefs.get(Constants.USERNAME);
    emailController.text = Preference.prefs.get(Constants.USEREMAIL);
    phoneController.text = "+91 2569 259 63256";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: LayoutConstant.containerDecoration(),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          leading: InkResponse(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Card(
                child: Icon(Icons.arrow_back),
                margin: EdgeInsets.all(10),
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.w),
                )),
          ),
          title: HeadlineText(
              text: AppString.editProfile, fontWeight: FontWeight.w500),
        ),
        bottomNavigationBar: Padding(
          padding: EdgeInsets.only(
              bottom: 40,
              left: MediaQuery.of(context).size.height * 0.05,
              right: MediaQuery.of(context).size.height * 0.05),
          child: ButtonSave(
              text: AppString.save,
              onPressed: () {
                Preference.prefs.setString(Constants.USERNAME, fullNameController.text);
                Preference.prefs.setString(Constants.USEREMAIL, emailController.text);
              },
              padding: EdgeInsets.symmetric(vertical: 15)),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.height * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Center(
                child: Container(
                  margin: EdgeInsets.only(right: 10.w, left: 10.w, top: 30.h),
                  height: MediaQuery.of(context).size.height * 0.18,
                  width: MediaQuery.of(context).size.width * 0.42,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      shape: BoxShape.rectangle,
                      image: DecorationImage(
                          fit: BoxFit.contain,
                          image: Preference.prefs.get(Constants.USERPROFILE) !=
                                  null
                              ? CachedNetworkImageProvider(
                                  Preference.prefs.get(Constants.USERPROFILE))
                              : AssetImage('images/user.png'))),
                  child: Stack(
                    children: [
                      Container(),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          margin: EdgeInsets.all(6),
                          decoration: BoxDecoration(
                              color: ColorConstant.buttonColor,
                              border: Border.all(color: Colors.white, width: 2),
                              shape: BoxShape.circle),
                          padding: EdgeInsets.all(8),
                          child: Icon(
                            Icons.camera_alt_rounded,
                            color: Colors.white,
                            size: 20,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.08),
              Theme(
                data: new ThemeData(
                  primaryColor: Colors.transparent,
                  hintColor: Colors.transparent,
                ),
                child: new TextField(
                  controller: fullNameController,
                  maxLines: 1,
                  style: GoogleFonts.rubik(textStyle: Theme.of(context).textTheme.bodyText2),
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),

                      hintText: 'Full Name',
                      hintStyle: GoogleFonts.rubik(
                          textStyle: Theme.of(context).textTheme.bodyText2),
                      filled: true,
                      fillColor: ColorConstant.cardBG),
                ),
              ),
              SizedBox(height: 14),
              Theme(
                data: new ThemeData(
                  primaryColor: Colors.transparent,
                  hintColor: Colors.transparent,
                ),
                child: new TextField(
                  maxLines: 1,
                  controller: emailController,
                  style: GoogleFonts.rubik(textStyle: Theme.of(context).textTheme.bodyText2),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                      hintText: 'Enter Your Email',
                      hintStyle: GoogleFonts.rubik(
                          textStyle: Theme.of(context).textTheme.bodyText2),
                      filled: true,
                      fillColor: ColorConstant.cardBG),
                ),
              ),
              SizedBox(height: 14),
              Theme(
                data: new ThemeData(
                  primaryColor: Colors.transparent,
                  hintColor: Colors.transparent,
                ),
                child: new TextField(
                  maxLines: 1,
                  controller: phoneController,
                  style: GoogleFonts.rubik(textStyle: Theme.of(context).textTheme.bodyText2),
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.transparent,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      ),
                      hintText: 'Phone',
                      hintStyle: GoogleFonts.rubik(
                          textStyle: Theme.of(context).textTheme.bodyText2),
                      filled: true,
                      fillColor: ColorConstant.cardBG),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
