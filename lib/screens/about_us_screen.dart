import 'package:flutter/material.dart';
import 'package:tip_top/utils/app_strings.dart';
import 'package:tip_top/utils/layout_constant.dart';
import 'package:tip_top/utils/text_button_widgtes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AboutUsScreen extends StatefulWidget {
  @override
  _AboutUsScreenState createState() => _AboutUsScreenState();
}

class _AboutUsScreenState extends State<AboutUsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: LayoutConstant.containerDecoration(),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          leading: InkResponse(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Card(
                child: Icon(Icons.arrow_back),
                margin: EdgeInsets.all(10),
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.w),
                )),
          ),
          title: HeadlineText(
              text: AppString.aboutUs, fontWeight: FontWeight.w500),
        ),
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              HeadlineText(
                  text: 'What is Tiptop Grocery?',
                  fontWeight: FontWeight.w400),
              SizedBox(height: 20),
              Body1Text(text: AppString.aboutUsText,fontWeight: FontWeight.w400)
            ],
          ),
        ),
      ),
    ));
  }
}
