import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:tip_top/screens/about_us_screen.dart';
import 'package:tip_top/screens/terms_and_policy_screen.dart';
import 'package:tip_top/utils/app_strings.dart';
import 'package:tip_top/utils/color_constant.dart';
import 'package:tip_top/utils/constants.dart';
import 'package:tip_top/utils/layout_constant.dart';
import 'package:tip_top/utils/navigation_constant.dart';
import 'package:tip_top/utils/preference.dart';
import 'package:tip_top/utils/text_button_widgtes.dart';

import 'edit_profile_screen.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {


  final InAppReview _inAppReview = InAppReview.instance;
  String _appStoreId = 'com.tiptop.grocery';
  String _microsoftStoreId = '';
  bool _isAvailable;



  Future<void> _openStoreListing() => _inAppReview.openStoreListing(
    appStoreId: _appStoreId,
  );

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _inAppReview
          .isAvailable()
          .then(
            (bool isAvailable) => setState(
              () => _isAvailable = isAvailable && !Platform.isAndroid,
        ),
      )
          .catchError((_) => setState(() => _isAvailable = false));
    });
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: LayoutConstant.containerDecoration(),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          leading: InkResponse(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Card(

                child: Icon(Icons.arrow_back),
                margin: EdgeInsets.all(10),
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.w),
                )),
          ),
          title: HeadlineText(
              text: AppString.myProfile, fontWeight: FontWeight.w500),
        ),
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: ColorConstant.cardColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              margin: EdgeInsets.only(right: 15.w, left: 15.w, top: 80.h),
            ),
            Padding(
              padding: EdgeInsets.only(right: 40.w, left: 40.w, top: 10.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Card(
                    elevation: 15,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.w),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.all(10.w),
                          height: 120.h,
                          width: 100.w,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              shape: BoxShape.rectangle,
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                  image: Preference.prefs.get(Constants.USERPROFILE) !=
                                          null
                                      ? CachedNetworkImageProvider(Preference
                                          .prefs
                                          .get(Constants.USERPROFILE))
                                      : AssetImage('images/user.png'))),
                        ),
                        Expanded(
                          child: Container(
                            height: 120.h,
                            margin: EdgeInsets.only(left: 8.w),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Expanded(
                                    flex: 2,
                                    child: Center(
                                        child: Headline5Text(
                                          textAlign: TextAlign.start,
                                            text: Preference.getString(Constants.USERNAME),
                                            fontWeight: FontWeight.w500,
                                            maxLines: 2))),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                          height: 30.h,
                                          width: 110.w,
                                          child: Buttons(
                                              text: AppString.editProfile,
                                              onPressed: () {
                                                NavigationConstant.navigationPush(context, EditProfileScreen());
                                              })),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 10.h),
                  userDetail(title: "Email:", value: Preference.getString(Constants.USEREMAIL)),
                  SizedBox(height: 5.h),
                  userDetail(title: "Phone:", value: "+91 2569 259 63256"),
                  SizedBox(height: 18.h),
                  Padding(
                      padding:   EdgeInsets.only(left: 10.w),
                      child: HeadlineText(
                          text: AppString.helpAndFeedback,
                          fontWeight: FontWeight.w400)),
                  SizedBox(height: 18.h),
                  userDetailOption(title: AppString.feedBack),
                  userDetailOption(title: AppString.aboutUs,onTap: (){
                    NavigationConstant.navigationPush(context, AboutUsScreen());
                  }),
                  userDetailOption(title: AppString.tAndC,onTap: (){
                    NavigationConstant.navigationPush(context, TermsAndPolicyScreen(appTitle: AppString.tAndC));
                  }),
                  userDetailOption(title: AppString.privacyPolicy,onTap: (){
                    NavigationConstant.navigationPush(context, TermsAndPolicyScreen(appTitle: AppString.privacyPolicy));
                  }),
                  userDetailOption(title: AppString.rateApp,onTap: (){
                    _openStoreListing();
                  }),
                  SizedBox(height: 30.h),
                  Body1Text(text: AppString.copyRight,textAlign: TextAlign.center)
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }

  Widget userDetail({String title, String value}) {
    return Card(
      elevation: 0,
      child: Padding(
        padding:   EdgeInsets.symmetric(vertical: 16.h, horizontal: 14.w),
        child: Row(
          children: [
            Expanded(child: Body1Text(text: title,fontWeight: FontWeight.w400)),
            Expanded(
                flex: 2,
                child: Body1Text(text: value, textAlign: TextAlign.end,fontWeight: FontWeight.w400)),
          ],
        ),
      ),
    );
  }

  Widget userDetailOption({String title,GestureTapCallback onTap}) {
    return InkResponse(
      onTap: onTap,
      child: Card(
        elevation: 0,
        child: Padding(
          padding:   EdgeInsets.symmetric(vertical: 16.h, horizontal: 14.w),
          child: Row(
            children: [
              Expanded(child: Body1Text(text: title,fontWeight: FontWeight.w400)),
              SvgPicture.asset('images/next.svg', height: 20.h, width: 12.w),
              SizedBox(width: 6.w)
            ],
          ),
        ),
      ),
    );
  }
}
