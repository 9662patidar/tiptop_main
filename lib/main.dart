import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:tip_top/providers/firebase_message_provider.dart';
import 'package:tip_top/screens/life_cycle_manager.dart';
import 'package:tip_top/screens/splash_screen.dart';
import 'package:tip_top/utils/color_constant.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (_) => FirebaseMessageProvider()),
  ], child: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      allowFontScaling: false,
      designSize: Size(414, 896),
      child: LifeCycleManager(
        child: MaterialApp(
          title: 'Tiptop Grocery',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: ColorConstant.colorPrimary,
          ),
          home: SplashScreen(),
        ),
      ),
    );
  }
}
