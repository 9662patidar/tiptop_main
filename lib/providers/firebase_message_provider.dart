import 'package:flutter/material.dart';

class FirebaseMessageProvider with ChangeNotifier {
  String _deviceToken = "";

  String get deviceToken => _deviceToken;

  void setDeviceToken(String token) {
    _deviceToken = token;
    notifyListeners();
  }
}
