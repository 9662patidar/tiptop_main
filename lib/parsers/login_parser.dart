import 'dart:convert';
import 'package:http/http.dart';
import 'package:tip_top/utils/constants.dart';

class LoginParser {
  static Future postLogin(String url, Map json) async {
    try {
      final response = await post(url, body: jsonEncode(json), headers:
          { "Content-Type": "application/x-www-form-urlencoded",
          "Content-type": "application/json"});

      if (response.statusCode == 200) {
        Map body = jsonDecode(response.body);
        if (body != null) {
          // UserModel userModel =new UserModel.fromJson(body);
          // if (userModel!=null) {
          //   return Constants.resultInApi(userModel, false);
          // }else{
          //   return Constants.resultInApi("body doesn't contain code",true);
          // }
        } else {
          return Constants.resultInApi("body is null", true);
        }
      }
    } catch (e) {
      return Constants.resultInApi("errorStack = " + e.toString(), true);
    }
  }
}
