import 'dart:io';

import 'package:flutter/material.dart';

class Constants {

  static String appUrl = "http://tiptop.arraybit.com/APIs";

  static String USERID =  "USERID";
  static String USEREMAIL =  "USEREMAIL";
  static String USERNAME=  "USERNAME";
  static String USERINFO =  "USERINFO";
  static String USERPROFILE =  "USERPROFILE";
  static String APIKEY =  "AIzaSyBppOGn2puvgBoLAzm5wTMptOUdwILOaB8";

  static Future<bool> isInternetAvailable() async {
    try {
      /*var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile) {
        // I am connected to a mobile network.
        return true;
      } else if (connectivityResult == ConnectivityResult.wifi) {
        // I am connected to a wifi network.
        return true;
      }*/

      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  static progressDialog(bool isLoading, BuildContext context) {
    AlertDialog dialog = new AlertDialog(
      content: new Container(
          height: 40.0,
          child: new Center(
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new CircularProgressIndicator(),
                Padding(padding: EdgeInsets.only(left: 15.0)),
                new Text("Please wait")
              ],
            ),
          )),
      contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
    );
    if (!isLoading) {
      Navigator.of(context).pop();
    } else {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return dialog;
          });
    }
  }

  static resultInApi(var value, var isError) {
    Map<String, dynamic> map = {"isError": isError, "value": value};
    return map;
  }

  static bool isValidPhone(String phone) {
    bool isPhone = false;
    RegExp exp = new RegExp('^[0-9]{10}\$');
    Iterable<Match> matches = exp.allMatches(phone);
    for (Match m in matches) {
      m.group(0);
      isPhone = true;
    }
    return isPhone;
  }

  static bool isValidEmail(String email) {
    bool isEmail = false;
    RegExp exp = new RegExp("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
    Iterable<Match> matches = exp.allMatches(email);
    for (Match m in matches) {
      m.group(0);
      isEmail = true;
    }
    return isEmail;
  }
}
