import 'package:flutter/material.dart';

class LayoutConstant {
  static containerDecoration() {
    return BoxDecoration(
        gradient: LinearGradient(
            colors: [
              Color.fromRGBO(235, 238, 244, 1),
              Color.fromRGBO(235, 238, 244, 0)
            ],
            begin: Alignment.bottomCenter,
            end: Alignment.topRight,
            stops: [0.5, 1],
            tileMode: TileMode.clamp));
  }

  static showSnackBar(GlobalKey<ScaffoldState> key,String content,BuildContext context){
    key.currentState.showSnackBar(new SnackBar(
        content: new Text(content,
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: Colors.yellow))));
  }
}
