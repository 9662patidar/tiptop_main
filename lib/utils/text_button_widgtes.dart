import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'color_constant.dart';

class CaptionText extends StatelessWidget {
  final String text;
  final Color color;
  final TextAlign textAlign;
  final FontWeight fontWeight;

  CaptionText({Key key, this.text, this.color, this.textAlign, this.fontWeight}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: textAlign,
        style: GoogleFonts.rubik(
            textStyle:
                Theme.of(context).textTheme.caption.copyWith(color: color,fontWeight: fontWeight)));
  }
}
class Body2Text extends StatelessWidget {
  final String text;
  final Color color;
  final TextAlign textAlign;

  Body2Text({Key key, this.text, this.color, this.textAlign}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: textAlign,
        style: GoogleFonts.rubik(
            textStyle:
                Theme.of(context).textTheme.bodyText1.copyWith(color: color)));
  }
}
class Body1Text extends StatelessWidget {
  final String text;
  final Color color;
  final FontWeight fontWeight;
  final TextAlign textAlign;

  Body1Text({Key key, this.text, this.color, this.fontWeight, this.textAlign}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: textAlign,
        style: GoogleFonts.rubik(
            textStyle:
                Theme.of(context).textTheme.bodyText2.copyWith(color: color,fontWeight: fontWeight!=null?fontWeight:FontWeight.normal)));
  }
}

class SubTitleText extends StatelessWidget {
  final String text;
  final Color color;
  final FontWeight fontWeight;
  final TextAlign textAlign;

  SubTitleText({Key key, this.text, this.color, this.fontWeight, this.textAlign}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: textAlign,
        style: GoogleFonts.rubik(
            textStyle:
                Theme.of(context).textTheme.subtitle1.copyWith(color: color,fontWeight: fontWeight!=null?fontWeight:FontWeight.normal)));
  }
}

class TitleText extends StatelessWidget {
  final String text;
  final Color color;

  TitleText({Key key, this.text, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: GoogleFonts.rubik(
            textStyle:
                Theme.of(context).textTheme.title.copyWith(color: color)));
  }
}

class HeadlineText extends StatelessWidget {
  final String text;
  final Color color;
  final FontWeight fontWeight;

  HeadlineText({Key key, this.text, this.color, this.fontWeight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: GoogleFonts.rubik(
            textStyle: Theme.of(context).textTheme.headline.copyWith(
                color: color,
                fontWeight:
                    fontWeight != null ? fontWeight : FontWeight.normal)));
  }
}

class Headline5Text extends StatelessWidget {
  final String text;
  final Color color;
  final FontWeight fontWeight;
  final int maxLines;
  final TextAlign textAlign;

  Headline5Text({Key key, this.text, this.color, this.fontWeight, this.maxLines, this.textAlign})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: textAlign,
        maxLines: maxLines,
        style: GoogleFonts.rubik(
            textStyle: Theme.of(context).textTheme.headline5.copyWith(
                color: color,
                fontWeight:
                    fontWeight != null ? fontWeight : FontWeight.bold)));
  }
}

class Buttons extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final EdgeInsetsGeometry padding;

  Buttons({Key key, this.text,  this.onPressed, this.padding}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        color:ColorConstant.buttonColor,
        padding: padding,
        child: CaptionText(text: text, color: Colors.white,fontWeight: FontWeight.w500),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: BorderSide(color: ColorConstant.buttonColor)),
        onPressed: onPressed);
  }
}

class ButtonSave extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final EdgeInsetsGeometry padding;

  ButtonSave({Key key, this.text,  this.onPressed, this.padding}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        color:ColorConstant.buttonColor,
        padding: padding,
        child: SubTitleText(text: text.toUpperCase(), color: Colors.white,fontWeight: FontWeight.w400),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: BorderSide(color: ColorConstant.buttonColor)),
        onPressed: onPressed);
  }
}
